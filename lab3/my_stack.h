#pragma once
#include <iostream>
#include "bintree.h"

using namespace std;

struct my_stack {
	bintree *data;
	my_stack *next;
};
void push(my_stack *&stack, bintree *new_element);
void pop(my_stack *&stack);
bintree *top(my_stack *stack);
void reverse(my_stack *&stack);
void write(my_stack *&stack);
void clear(my_stack *&stack);