#pragma once
#include <iostream>
#include <iomanip>
using namespace std;

struct bintree {
	int data;
	bintree *left;
	bintree *right;
};
int irand(int, int);
void push(bintree*&, int);
void create(bintree*&, int);
void write(bintree*);
void buity_write(bintree*, int = 0);
bintree* find(bintree*, int);
int size(bintree*);
int countLeaf(bintree*);
int power(bintree*, int);				
int level(bintree*, int, int level = 0);			
int high(bintree*, int height = 0);	
bintree* universal_find(bintree*, int);	
void erase_subtree(bintree*&, int);
void clear(bintree*&);					
void erase_node(bintree*&, int);
void tree_traversal_high(bintree*);	
void tree_traversal_width(bintree*);


void right_rotation(bintree*&);//
void left_rotation(bintree*&);//
void tree_to_array(bintree*, int*, int*);
void tree_balance(bintree*&);			//
