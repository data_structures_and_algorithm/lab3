#include "my_queue.h"

void generate(my_queue *&queue, bintree *data)
{
	queue = new my_queue;
	queue->first = new queue_node;
	queue->first->data = data;
	queue->last = queue->first;
	queue->last->next = 0;
}
void push(my_queue *&queue, bintree *data)
{
	if (!queue) {
		generate(queue, data);
		return;
	}
	queue_node *p = new queue_node;
	p->data = data;
	p->next = 0;
	queue->last->next = p;
	queue->last = p;
}
void pop(my_queue *&queue)
{
	if (!queue)
		return;
	queue_node *p = queue->first;
	queue->first = queue->first->next;
	delete p;
	if (!queue->first)
		queue = 0;
}
void write(my_queue *queue)
{
	if (!queue)
		return;
	queue_node *p = queue->first;
	while (p) {
		cout << p->data << " ";
		p = p->next;
	}
	cout << endl;
}
bintree *top(my_queue *queue)
{
	if (!queue)
		return 0;
	//queue_node *p = queue->first;
	//queue->first = queue->first->next;
	return queue->first->data;
}