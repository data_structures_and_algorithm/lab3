#pragma once
#include <iostream>
#include "bintree.h"

using namespace std;

struct queue_node {
	bintree *data;
	queue_node *next;
};
struct my_queue {
	queue_node *first;
	queue_node *last;
};
void generate(my_queue *&queue, bintree *data);
void push(my_queue *&queue, bintree *data);
void pop(my_queue *&queue);
bintree *top(my_queue *queue);
void write(my_queue *queue);