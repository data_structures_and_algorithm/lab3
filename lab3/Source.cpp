#include <iostream>
#include <time.h>
#include "bintree.h"
#include "my_stack.h"

//#include "C:\Users\Alex\source\repos\data_structures_and_algorithm\lab2\lab3\my_stack.h"
//#include "C:\Users\Alex\source\repos\data_structures_and_algorithm\lab2\lab3\my_queue.h"
using namespace std;
void print_Tree(bintree * p, int level)
{
	if (p)
	{
		print_Tree(p->left, level + 1);
		for (int i = 0; i < level; i++) cout << "   ";
		cout << p->data << endl;
		print_Tree(p->right, level + 1);
	}
}
void write()
{
	cout << "������� ������ �� n ��-���: "		<< 1;
	cout << "���������� ��������: "				<< 2;
	cout << "����� �� �����������: "			<< 3;
	cout << "\"��������\" �����: "				<< 4;
	cout << "����� � ������ ������: "			<< 5;
	cout << "����� � ������� ������: "			<< 6;
	cout << "����� ����� ������: "				<< 7;
	cout << "����� ������� ������: "			<< 8;
	cout << "������� �������: "					<< 9;
	cout << "������� �������: "					<< 10;
	cout << "������ ������: "					<< 11;
	cout << "�������� ���� � ����������: "		<< 12;
	cout << "�������� ������: "					<< 13;
	cout << "�������� ���� � ������������: "	<< 14;
	cout << "����� � �������(�� �����������): " << 15;
	cout << "����� � ������: "					<< 16;
	cout << "������������: "					<< 17;
}

int main()
{
	srand(unsigned(time(0)));
	bintree *Tree = 0;
	int get = 1, n;
	while (get) {
		cout << "Write command: ";
		cin >> get;
		switch (get) {
		case 1: {
			cin >> n;
			create(Tree, n);
			break;
		}
		case 2: {
			cin >> n;
			push(Tree, n);
			break;
		}
		case 3: {
			write(Tree);
			cout << endl;
			break;
		}
		case 4:	 {
			buity_write(Tree);
			break;
			}
		case 5:	 {
			cin >> n;
			buity_write(find(Tree, n));
			break;
			}
		case 6:	 {
			cin >> n;
			buity_write(universal_find(Tree, n));
			break;
			}
		case 7:	 {
			cout << "Tree size: " << size(Tree) << endl;
			break;
			}
		case 8:	 {
			cout << "Count of leafs: " << countLeaf(Tree) << endl;
			break;
			}
		case 9:	 {
			cin >> n;
			cout << "Power of node: " << power(Tree, n) << endl;
			break;
			}
		case 10: {
			cin >> n;
			cout << "Level of node: " << level(Tree, n) << endl;
			break;
			}
		case 11: {
			cout << "Hight of tree: " << high(Tree) << endl;
			break;
			}
		case 12: {
			cin >> n;
			erase_subtree(Tree, n);
			buity_write(Tree);
			break;
			}
		case 13: {
			clear(Tree);
			buity_write(Tree);
			break;
			}
		case 14: {
			cin >> n;
			erase_node(Tree, n);
			buity_write(Tree);
			break;
			}
		case 15: {
			tree_traversal_high(Tree);
			break;
			}
		case 16: {
			tree_traversal_width(Tree);
			cout << endl;
			break;
			}
		case 17: {
			tree_balance(Tree);
			cout << endl;
			break;
			}
		default: get = 0;
		}
	}
	system("pause");
	return 0;
}
