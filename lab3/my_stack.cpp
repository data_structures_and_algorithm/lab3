#include "my_stack.h"

void push(my_stack *&stack, bintree *new_element)
{
	my_stack *p = new my_stack;
	p->data = new_element;
	p->next = stack;
	stack = p;
}
void pop(my_stack *&stack)
{
	if (!stack)
		return;
	my_stack *p = stack;
	stack = stack->next;
	delete p;
}
bintree *top(my_stack *stack) 
{
	return stack->data;
}
void reverse(my_stack *&stack)
{
	my_stack *p = 0, *w;
	while (stack) {
		w = new my_stack;
		w->data = stack->data;
		w->next = p;
		p = w;
		w = stack;
		stack = stack->next;
		delete w;
	}
	stack = p;
}
void write(my_stack *&stack)
{
	reverse(stack);
	bintree *data;
	my_stack *p = 0, *w;
	while (stack) {
		data = stack->data;
		cout << data << " ";
		w = new my_stack;
		w->data = data;
		w->next = p;
		p = w;
		w = stack;
		stack = stack->next;
		delete w;
	}
	stack = p;
	cout << endl;
}
void clear(my_stack *&stack)
{
	while (stack)
		pop(stack);
}