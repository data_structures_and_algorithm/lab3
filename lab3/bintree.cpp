#include "bintree.h"
#include "my_queue.h"
#include "my_stack.h"

int irand(int a, int b) { return rand() % (b - a + 1) + a; }
void push(bintree *&tree, int data)
{
	if (!tree) {
		tree = new bintree;
		tree->data = data;
		tree->left = 0;
		tree->right = 0;
		return;
	}
	if (tree->data < data)
		push(tree->right, data);
	if (tree->data > data)
		push(tree->left, data);
}
void create(bintree *&tree, int treeSize)
{
	tree = new bintree;
	tree->data = irand(-999, 9999);
	tree->left = 0;
	tree->right = 0;
	int el;
	bintree *p = tree;
	//for (int i = 0; i < size - 1; ++i) {
	//	el = irand(-99, 100);
	//	push(tree, el);
	//}
	while (size(tree) < treeSize)
		push(tree, irand(-999, 9999));
}
void write(bintree *tree)
{
	if (!tree) return;
	write(tree->left);
	cout << tree->data << " ";
	write(tree->right);
}
void buity_write(bintree *tree, int lev)
{
	//int hight = high(tree), width = (1 << (hight + 2)) + 3, size = (1 << (hight + 1)) - 2, sum = hight * (hight - 1) / 2;
	//my_queue *q = 0;
	//bintree *p;
	//push(q, tree);
	//for (int i = hight; i >= 0; --i) {
	//	for (int j = 0; j < (1 << (hight - i)); ++j) {
	//		p = top(q);
	//		if (p) {
	//			cout << setfill(' ') << setw(width / ((1 << (hight - i)) + 1)) << p->data;
	//			push(q, p->left);
	//			push(q, p->right);
	//		}
	//		else {
	//			cout << setfill(' ') << setw(width / ((1 << (hight - i)) + 1)) << "   ";
	//			push(q, 0);
	//			push(q, 0);
	//		}
	//		pop(q);
	//	}
	//	sum -= i;
	//	cout << endl;
	//}
	const int numWidth = 4;
	if (!tree) return;
	if (!lev) {
		cout << setfill(' ') << setw(numWidth) << 0;
		for (int i = 1; i <= high(tree); ++i) cout << setfill(' ') << setw(3 + numWidth) << i;
		cout << endl;
	}
	//cout << endl;
	buity_write(tree->right, lev + 1);
	cout << setfill(' ') << setw((3 + numWidth) * lev + numWidth) << tree->data << endl;
	buity_write(tree->left, lev + 1);
}
bintree* find(bintree *tree, int data)
{
	if (!tree)
		return 0;
	if (data == tree->data)
		return tree;
	if (data < tree->data)
		find(tree->left, data);
	else
		find(tree->right, data);
}
int size(bintree *tree)
{
	if (!tree)
		return 0;
	return size(tree->left) + size(tree->right) + 1;
}
int countLeaf(bintree *tree)
{
	if (!tree)
		return 0;
	if (!tree->left && !tree->right)
		return 1;
	return countLeaf(tree->left) + countLeaf(tree->right);
}
int power(bintree *tree, int data)
{
	tree = find(tree, data);
	if (!tree)
		return -1;
	int a = !!(tree->left) + !!(tree->right);
	return a;
}
int level(bintree *tree, int data, int lev)
{
	if (!tree)
		return -1;
	if (data == tree->data)
		return lev;
	int r = -1, l = -1;
	if (data < tree->data)
		l = level(tree->left, data, lev + 1);
	else
		r = level(tree->right, data, lev + 1);
	return (l != -1) ? l : r;
}
int high(bintree *tree, int hight)
{
	if (!tree)
		return hight - 1;
	int m, n;
	m = high(tree->left, hight + 1);
	n = high(tree->right, hight + 1);
	n = (n < m) ? m : n;
	return n;
}
bintree* universal_find(bintree *tree, int data)
{
	if (!tree)
		return 0;
	if (data == tree->data)
		return tree;
	bintree *r, *l;
	l = universal_find(tree->left, data);
	r = universal_find(tree->right, data);
	if (l)
		return l;
	return r;
}
//void erase_subtree(bintree*& tree, int data)
//{
//	if (!tree)
//		return;
//	if (tree->data == data) {
//		clear(tree);
//		return;
//	}
//	bool direction;
//	bintree *cur = tree, *prev = tree;
//	while (cur && cur->data != data) {
//		prev = cur;
//		if (cur->data > data) {
//			direction = 0;
//			cur = cur->left;
//		} else
//			if (cur->data < data) {
//				direction = 1;
//				cur = cur->right;
//			}
//	}
//	clear(cur);
//	if (direction)
//		prev->right = cur;
//	else
//		prev->left = cur;
//
//}
void erase_subtree(bintree*& tree, int data)
{
	if (!tree)
		return;
	if (data > tree->data)
		erase_subtree(tree->right, data);
	if (data < tree->data)
		erase_subtree(tree->left, data);
	if (tree->data == data) {
		clear(tree);
		return;
	}
}
void clear(bintree *&tree)
{
	if (!tree)
		return;
	clear(tree->left);
	clear(tree->right);
	delete tree;
	tree = 0;
}
void erase_node(bintree *&tree, int data)
{
	if (!tree) return;
	bool direction;
	bintree *cur = tree, *prev = tree, *t;
	while (cur && cur->data != data) {
		prev = cur;
		if (cur->data > data) {
			cur = cur->left;
			direction = 0;
		} 
		else 
			if (cur->data < data) {
				cur = cur->right;
				direction = 1;
			}
	}
	if (!cur) return;
	//���� ����� � ������ � ������� �������
	if (cur->right && cur->left) {
		prev = cur;
		t = cur->left;
		if (t->right)
			direction = 1;
		else 
			direction = 0;
		while (t->right) {
			prev = t;
			t = t->right;
		}
		if (direction)
			prev->right = t->left;
		else
			prev->left = t->left;
		cur->data = t->data;
		delete t;
		return;
	}
	//���� �� ����� ��������
	if (!cur->right && !cur->left) {
		if (direction)
			prev->right = 0;
		else
			prev->left = 0;
		clear(cur);
		return;
	}
	//���� ����� ������ ������ �������
	if (!cur->right && cur->left) {
		if (direction)
			prev->right = cur->left;
		else
			prev->left = cur->left;
		delete cur;
		return;
	}
	//���� ����� ������ ������� �������
	if (cur->right && !cur->left) {
		if (direction)
			prev->right = cur->right;
		else 
			prev->left = cur->right;
		delete cur;
		return;
	}
}
void tree_traversal_high(bintree *tree)
{
	my_stack *tree_stack = 0;
	push(tree_stack, tree);
	while (tree_stack) {
		tree = top(tree_stack);
		pop(tree_stack);
		if (tree) {
			cout << tree->data << " ";
			push(tree_stack, tree->right);
			push(tree_stack, tree->left);
		}
	}
}
void tree_traversal_width(bintree *tree)
{
	my_queue *queue = 0;
	push(queue, tree);
	while (queue) {
		tree = top(queue);
		pop(queue);
		if (tree) {
			cout << tree->data << " ";
			push(queue, tree->right);
			push(queue, tree->left);
		}
	}

}

void right_rotation(bintree *&tree)
{

}
void left_rotation(bintree *&tree)
{
	//if (!(tree && tree->left && tree->left->right)) {
	//	cout << "Re-re";
	//	return;
	//}
	//cout << "Er-er";
	//bintree *p = tree->left, *q = p->right;
	//p->right = q->left;
	
}
void tree_to_array(bintree *tree, int *arr, int *idx)
{
	if (!tree) return;
	tree_to_array(tree->left, arr, idx);
	arr[*idx] = tree->data;
	(*idx)++;
	tree_to_array(tree->right, arr, idx);
}
void parse(int left, int right, int *arr, bintree *&tree)
{
	if (left == right)
		push(tree, arr[left]);
	if (left + 1 == right) {
		push(tree, left);
		push(tree, right);
	}
	parse(left, right / 2, arr, tree);
	parse(right / 2, right, arr, tree);
}
void tree_balance(bintree *&tree)
{
	bintree *p = tree, *q, *t;
	if (!tree || !power(tree, tree->data)) return;
	//�������������� ������ � ����
	while (tree->right) {
		p = tree->right;
		tree->right = p->left;
		p->left = tree;
		tree = p;
	}
	while (p->left) {
		q = p;
		p = p->left;
		while (p->right) {
			t = p->right;
			p->right = t->left;
			t->left = p;
			p = t;
		}
		q->left = p;
	}
	//�������������� ���� � ���������������� ������
	bintree *root = tree;
	int vineSize = size(tree), balancedLevel = 0, shorter = 1;
	while (vineSize > (1 << balancedLevel++));
	--balancedLevel;
	if (vineSize != (1 << balancedLevel) - 1) {
		--balancedLevel;
		shorter += vineSize - (1 << (balancedLevel)) - 1;
	}
	for (int i = 2; i < balancedLevel; ++i) {
		p = root;
		t = p->left;
		p->left = t->right;
		t->right = p;
		root = t;
		q = root;
		for (int i = 0; i < vineSize / 2 - shorter; ++i) {
			p = q->left;
			t = p->left;
			p->left = t->right;
			t->right = p;
			q->left = t;
			q = q->left;
		}
		vineSize /= 2;
	}
	p = root;
	t = p->left;
	p->left = t->right;
	t->right = p;
	root = t;
	q = root;
	tree = root;
}